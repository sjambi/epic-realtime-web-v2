package controllers

import javax.inject.Inject

import akka.NotUsed
import akka.stream.scaladsl.{Source, _}
import akka.util._
import play.api.libs.EventSource
import play.api.libs.json._
import play.api.libs.ws.{WSClient, WSRequest}
import play.api.mvc._

import scala.concurrent.ExecutionContext
import scala.concurrent.duration._

import scala.concurrent.{Future, Await, future}
// import scala.concurrent.ExecutionContext.Implicits.global

// import models.QueryView

case class TweetCount(count: String)
object TweetCount { implicit val TweetCountFormat = Json.format[TweetCount]}
case class CountResult(message: TweetCount)
object CountResult { implicit val countresultFormat: Format[CountResult] = Json.format[CountResult]}

case class QueryView(value: String, count: Int)
object QueryView { implicit val queryViewFormat = Json.format[QueryView]}
case class QueryResult(message: Seq[QueryView])
object QueryResult { implicit val queryResultFormat: Format[QueryResult] = Json.format[QueryResult]}

case class PopularQueryView(count: String, value: String, screen_name:String, text:String)
object PopularQueryView { implicit val popularQueryViewFormat = Json.format[PopularQueryView]}
case class PopularQueryResult(message: Seq[PopularQueryView])
object PopularQueryResult { implicit val popularQueryResultFormat: Format[PopularQueryResult] = Json.format[PopularQueryResult]}

case class DatasetQueryView(value: String)
object DatasetQueryView { implicit val datasetQueryViewFormat = Json.format[DatasetQueryView]}
case class DatasetQueryResult(message: Seq[DatasetQueryView])
object DatasetQueryResult { implicit val datasetQueryResultFormat: Format[DatasetQueryResult] = Json.format[DatasetQueryResult]}


class Streaming @Inject()(wSClient: WSClient)(implicit ec: ExecutionContext) extends Controller {

  val eventHost = "localhost"
  val eventPort = "5000"
  val queryHost = "localhost"
  val queryPort	= "5001"


  def counttimeline(event_name: String) = Action {
    val source = Source.tick(initialDelay = 0.second, interval = 0.001.second, tick = "tick")
    Ok.chunked(source.map { tick =>
      // lazy val futureList =  epicRealTimeDB.getAll.map(result => Json.toJson(result))//.flatten.mkString(", ")    
      lazy val responseFuture = wSClient.url(s"http://$eventHost:$eventPort/events/${event_name}/count").get
      val response = Await.result(responseFuture, 10 seconds)
      val tweet_count = Json.parse(response.body).as[TweetCount]
      Json.obj("message" -> tweet_count)
      //http://stackoverflow.com/questions/17683939/serializing-a-scala-list-to-json-in-play2
     })
  }

  def countchunked(event_name: String) = Action {
    val responses = sendCountToSource(event_name)
    Ok.chunked(responses via EventSource.flow)
  }

  private def sendCountToSource(event_name: String) = {
    val request = wSClient
      .url(s"http://localhost:9000/counttimeline")
      .withQueryString("event_name" -> event_name)

    streamResponse(request)
      // .via(framing)
      .map { byteString =>
        val json = Json.parse(byteString.utf8String)
        val queryResult = CountResult((json \ "message").as[TweetCount])
        Json.toJson(queryResult)
      }
  }


  val framing = Framing.delimiter(ByteString("\n"), maximumFrameLength = 100, allowTruncation = true)

  def querytimeline(event: String, querytype: String, lambdatype: String, query: String) = Action {
    val source = Source.tick(initialDelay = 0.second, interval = 0.02.second, tick = "tick")
    Ok.chunked(source.map { tick =>
      // lazy val futureList =  epicRealTimeDB.getAll.map(result => Json.toJson(result))//.flatten.mkString(", ")    
      lazy val responseFuture = wSClient.url(s"http://$queryHost:$queryPort/events/$event/$querytype/$lambdatype/query/$query/result").get
      // println(responseFuture)
      val response = Await.result(responseFuture, 10 seconds)
      val output_list = (Json.parse(response.body)).as[Seq[QueryView]]
      // if (output_list.isEmpty)
      // println(output_list)
      Json.obj("message" -> output_list)
      // .toString + "\n" 

      //http://stackoverflow.com/questions/17683939/serializing-a-scala-list-to-json-in-play2
     }.limit(1000))
  }
  def querychunked(event: String, querytype: String, lambdatype: String, query: String) = Action {
    val responses = sendQueryToSource(event, querytype, lambdatype, query)
    Ok.chunked(responses via EventSource.flow)
  }
  private def sendQueryToSource(event: String, querytype: String, lambdatype: String, query: String) = {
    val request = wSClient
      .url(s"http://localhost:9000/querytimeline")
      .withQueryString("event" -> event)
      .withQueryString("querytype" -> querytype)      
      .withQueryString("lambdatype" -> lambdatype)
      .withQueryString("query" -> query)
    streamResponse(request)
      // .via(framing)
      .map { byteString =>
        val json = Json.parse(byteString.utf8String)
        val queryResult = QueryResult((json \ "message").as[Seq[QueryView]])
        Json.toJson(queryResult)
      }
  }


def datasetquerytimeline(event: String, querytype: String, lambdatype: String, query: String) = Action {
    val source = Source.tick(initialDelay = 0.second, interval = 1.second, tick = "tick")
    Ok.chunked(source.map { tick =>
      // lazy val futureList =  epicRealTimeDB.getAll.map(result => Json.toJson(result))//.flatten.mkString(", ")    
      lazy val responseFuture = wSClient.url(s"http://$queryHost:$queryPort/events/$event/$querytype/$lambdatype/query/$query/result").get
      val response = Await.result(responseFuture, 10 seconds)
      val output_list = (Json.parse(response.body)).as[Seq[DatasetQueryView]]
      Json.obj("message" -> output_list)
      //http://stackoverflow.com/questions/17683939/serializing-a-scala-list-to-json-in-play2
     }.limit(1000))
  }
  def datasetquerychunked(event: String, querytype: String, lambdatype: String, query: String) = Action {
    val responses = sendDatasetQueryToSource(event, querytype, lambdatype, query)
    Ok.chunked(responses via EventSource.flow)
  }
  private def sendDatasetQueryToSource(event: String, querytype: String, lambdatype: String, query: String) = {
    val request = wSClient
      .url(s"http://localhost:9000/datasetquerytimeline")
      .withQueryString("event" -> event)
      .withQueryString("querytype" -> querytype)      
      .withQueryString("lambdatype" -> lambdatype)
      .withQueryString("query" -> query)
    streamResponse(request)
      .map { byteString =>
        val json = Json.parse(byteString.utf8String)
        val queryResult = DatasetQueryResult((json \ "message").as[Seq[DatasetQueryView]])
        Json.toJson(queryResult)
      }
  }


  def popularquerytimeline(event: String, querytype: String, lambdatype: String, query: String) = Action {
    val source = Source.tick(initialDelay = 0.second, interval = 1.second, tick = "tick")
    Ok.chunked(source.map { tick =>
      // lazy val futureList =  epicRealTimeDB.getAll.map(result => Json.toJson(result))//.flatten.mkString(", ")    
      lazy val responseFuture = wSClient.url(s"http://$queryHost:$queryPort/events/$event/$querytype/$lambdatype/query/$query/result").get
      val response = Await.result(responseFuture, 10 seconds)
      val output_list = (Json.parse(response.body)).as[Seq[PopularQueryView]]
      Json.obj("message" -> output_list)
      //http://stackoverflow.com/questions/17683939/serializing-a-scala-list-to-json-in-play2
     }.limit(1000))
  }
  private def sendPopularQueryToSource(event: String, querytype: String, lambdatype: String, query: String) = {
    val request = wSClient
      .url(s"http://localhost:9000/popularquerytimeline")
      .withQueryString("event" -> event)
      .withQueryString("querytype" -> querytype)      
      .withQueryString("lambdatype" -> lambdatype)
      .withQueryString("query" -> query)
    streamResponse(request)
      .map { byteString =>
        val json = Json.parse(byteString.utf8String)
        val queryResult = PopularQueryResult((json \ "message").as[Seq[PopularQueryView]])
        Json.toJson(queryResult)
      }
  }
  def popularquerychunked(event: String, querytype: String, lambdatype: String, query: String) = Action {
    val responses = sendPopularQueryToSource(event, querytype, lambdatype, query)
    Ok.chunked(responses via EventSource.flow)
  }


  private def streamResponse(request: WSRequest) = Source.fromFuture(request.stream()).flatMapConcat(_.body)

}
