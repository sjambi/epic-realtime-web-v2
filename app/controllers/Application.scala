package controllers

import models.Event
import models.Keyword
import models.Active
import models.Query
import models.CreateQueryForm
import models.Process
import models.ProcessRequest

import play.api._
import play.api.data._
import play.api.data.Forms._
import play.api.i18n.{I18nSupport, MessagesApi}
import play.api.mvc._
// import play.filters.csrf.CSRFConfig
import scala.collection.mutable.ArrayBuffer
import javax.inject.Inject

import akka.NotUsed
import akka.stream.scaladsl.{Source, _}
import akka.util._
import play.api.libs.EventSource
import play.api.libs.json._
import play.api.libs.ws.{WSClient, WSRequest, WSResponse}
import scala.concurrent.ExecutionContext.Implicits.global

import scala.concurrent.ExecutionContext
import scala.concurrent.duration._
import scala.concurrent._

import java.util.concurrent.Executors
// import scala.util.{Success, Failure, Try}

import play.api.Play.current
import play.api.i18n.Messages.Implicits._

// import scala.async.Async.{async, await}
// import play.api.cache.Cached

class Application @Inject()(wSClient: WSClient)(implicit ec: ExecutionContext) extends Controller {

  // implicit val ec: ExecutionContext

  val eventHost = "localhost"
  val eventPort = "5000"
  val queryHost = "localhost"
  val queryPort = "5001"
 
  private val events = ArrayBuffer.empty[Event]
  private val keywords = ArrayBuffer.empty[Keyword]
  private val processes = ArrayBuffer.empty[Process]
  private val queries = ArrayBuffer.empty[Query]


  def index = Action {
    val count = countEvents
    Ok(views.html.index(s"Number of Events: $count"))
  }

  def listEvents = //Cached { 
    Action.async { implicit request =>
      val responseFuture =
        wSClient.url(s"http://$eventHost:$eventPort/events")
        .get
        responseFuture.map { response =>
          val events_list = Json.parse(response.body).\("events").as[Seq[Event]]
          Ok(views.html.eventList(events_list))
        }
    }
  

  def showEventCount = Action { implicit request =>
    Ok(countEvents())
  }

  def countEvents(): String = {
    val responseFuture = wSClient
      .url(s"http://$eventHost:$eventPort/events").get
    val response = Await.result(responseFuture, 100 seconds) 
    val events_list = Json.parse(response.body).\("events").as[Seq[Event]]
    return events_list.length.toString
  }

  def asynclistEvents = Action.async{ request =>
    val futureJsUserArray = wSClient.url(s"http://$eventHost:$eventPort/events").get()
    futureJsUserArray.map{jsResponse => Ok(jsResponse.body)}//.as("application/json")}
  }


  def getEventForm = Action { implicit request =>
    Ok(views.html.addEvent(Application.addEventForm))
  }

  // This will be the action that handles our form post
  def addEvent = Action { implicit request =>
    val formValidationResult = Application.addEventForm.bindFromRequest
    formValidationResult.fold(
      formWithErrors => {
        // binding failure, you retrieve the form containing errors:
        BadRequest(views.html.addEvent(formWithErrors))
      }, 
      event => {
        // binding success, you get the actual value.
        //events.append(event)          
        val newEvent = Json.toJson(Map(
          "description" -> event.description,
          "startdate" -> event.startDate
        ))
        val responseFuture = wSClient.url(s"http://$eventHost:$eventPort/events/${event.name}").post(newEvent)
        Redirect(routes.Application.listEvents) //Problem: needs refresh
      }
    )
  }

  def getEvent(id: String) = Action.async { implicit request =>
    val responseFuture = wSClient.url(s"http://$eventHost:$eventPort/events/$id").get
      responseFuture.map { response =>
        val eventFound = Json.parse(response.body).as[Event]
        val keywords_json = getEventKeywords(id)
        val keywords = (keywords_json).as[Seq[Keyword]]
        val queries = ArrayBuffer.empty[Query]
        queries ++= getEventQueries(id, "trend", "hashtag").as[Seq[Query]] 
        queries ++= getEventQueries(id, "trend", "user").as[Seq[Query]] 
        queries ++= getEventQueries(id, "trend", "url").as[Seq[Query]] 
        queries ++= getEventQueries(id, "trend", "mention").as[Seq[Query]]
        queries ++= getEventQueries(id, "popular", "tweet").as[Seq[Query]]
        queries ++= getEventQueries(id, "popular", "user").as[Seq[Query]]
        queries ++= getEventQueries(id, "dataset", "hashtag").as[Seq[Query]]
        queries ++= getEventQueries(id, "dataset", "user").as[Seq[Query]]
        queries ++= getEventQueries(id, "dataset", "mention").as[Seq[Query]]
        queries ++= getEventQueries(id, "dataset", "url").as[Seq[Query]]
        Ok(views.html.event(eventFound,keywords,processes,queries))   
      }     
  }

  def getKeywordForm(id: String) = Action { implicit request =>
    Ok(views.html.addKeyword(id, Application.addKeywordForm))
  }

  def addKeyword(id: String) = Action { implicit request =>
    val formValidationResult = Application.addKeywordForm.bindFromRequest
    formValidationResult.fold(
      formWithErrors => {
        // binding failure, you retrieve the form containing errors:
        BadRequest(views.html.addKeyword(id, formWithErrors))
      }, 
      keyword => {
        // binding success, you get the actual value.
        val keywordEntry = Json.toJson(Map(
          "word" -> keyword.text
        ))
        val responseFuture = wSClient.url(s"http://$eventHost:$eventPort/events/$id/keywords").post(keywordEntry)
        Redirect(routes.Application.getEvent(id)) //Problem: needs refresh
      }
    )
  }

  def listKeywords(id: String) = Action.async { implicit request =>
    val responseFuture = wSClient.url(s"http://$eventHost:$eventPort/events/$id/keywords").get
      responseFuture.map { response => 
        val keywords = Json.parse(response.body).\("keywords").as[Seq[Keyword]]
        Ok(views.html.keywordList(id,keywords))
      }
  }

  private def getEventKeywords(id: String) = {
    val responseFuture = wSClient
      .url(s"http://$eventHost:$eventPort/events/$id/keywords").get
    val response = Await.result(responseFuture, 100 seconds) 
    val keywords = Json.parse(response.body).\("keywords").as[Seq[Keyword]]
    Json.toJson(keywords)
  }

  // private def getEventLambdas(id: String, querytype: String, lambdatype: String) = {
  //   var path =""
  //   querytype match{
  //     case "trend" => 
  //       path = s"http://$queryHost:$queryPort/events/$id/$querytype/$lambdatype"
  //     case "popular" =>
  //       path = s"http://$queryHost2:$queryPort2/events/$id/$querytype"
  //   }
  //   val responseFuture = wSClient.url(path).get
  //   val response = Await.result(responseFuture, 100 seconds) 
  //   if (response.status == 200) {
  //     val trendlambdas = Json.parse(response.body).as[Process]
  //     Json.toJson(trendlambdas)
  //   } 
  //   else{
  //     val json: JsValue = Json.parse("""{
  //     "event": "",
  //     "lambdatype": "",
  //     "querytype": "",
  //     "speed_job_running": false,
  //     "speed_job_id": "",
  //     "batch_job_running": false,
  //     "batch_job_id": ""
  //     }""")
  //     val trendlambdas = (json).as[Process]
  //     Json.toJson(trendlambdas)
  //   }
  // }

  private def getEventQueries(id: String, querytype: String, lambdatype: String) =  {
    val request = wSClient.url(s"http://$queryHost:$queryPort/events/$id/$querytype/$lambdatype/query/all").get()
    val response = Await.result(request, 100 seconds) 
    if (response.status == 200) {
      val queries = Json.parse(response.body).\("queries").as[Seq[Query]]
      Json.toJson(queries)
    } 
    else{
      val json: JsValue = Json.parse("""{"queries" : []}""")
      val queries = (json \"queries").as[Seq[Query]]
      Json.toJson(queries)
    }
  }
 
// def getEventTrendQueries(id: String, lambdatype: String) = Action.async {
//       val request = wSClient.url(s"http://$queryHost:$queryPort/events/$id/trend/$lambdatype/query/all").get()
//           // .withTimeout(3000).get
 
//       request map { response => 
//          if (response.status == 200) {
//             Ok(response.json)
//         } 
//         else{
//             Ok("Not Found")
//         }
//       } recover { 
//         case t: TimeoutException => 
//           RequestTimeout(t.getMessage)
//         case e =>
//           ServiceUnavailable(e.getMessage)
//       }
//   }
// https://groups.google.com/forum/#!topic/play-framework/UylCl0mAa00
  

  def count(event_name: String) = Action {
    Ok(views.html.count(event_name))
  }

  def getActiveForm(event_name: String) = Action { implicit request =>
    Ok(views.html.setActive(event_name, Application.addActiveForm))
  }

  def setActive(event_name: String) = Action { implicit request =>
    val formValidationResult = Application.addActiveForm.bindFromRequest
    formValidationResult.fold(
        // binding failure, you retrieve the form containing errors:
      errorForm => {
        Ok(views.html.index("Error in Set Event Active Status"))
      }, 
      status => {
        // binding success, you get the actual value.       
        val activeEntry = Json.toJson(Map(
          "active" -> status.active
        ))
        val responseFuture = wSClient.url(s"http://$eventHost:$eventPort/events/${event_name}/active").post(activeEntry)
        val response = Await.result(responseFuture, 100 seconds)
        Redirect(routes.Application.getEvent(event_name))
       }
    )
  }

  // def setProcessStatus(event_name: String, querytype: String, lambdatype: String) = Action { implicit request =>
  //  val formValidationResult = Application.setProcessForm.bindFromRequest
  //   formValidationResult.fold(
  //       // binding failure, you retrieve the form containing errors:
  //     errorForm => {
  //       Ok(views.html.index("Error in Set Process Status"))
  //     }, 
  //     process => {
  //       // binding success, you get the actual value. 
  //       var path =""
  //       querytype match{
  //         case "trend" =>
  //           path = s"http://$queryHost:$queryPort/events/${event_name}/$querytype/${lambdatype}/${process.request}"
  //         case "popular" =>
  //           path = s"http://$queryHost2:$queryPort2/events/${event_name}/$querytype/${process.request}"
  //       }
  //       val responseFuture = wSClient.url(path).post("")
  //       val response = Await.result(responseFuture, 100 seconds)
  //       Redirect(routes.Application.getEvent(event_name))
  //     }
  //   )
  // }


  def addQuery(event_name: String, querytype: String, lambdatype: String) = Action { implicit request =>
  // Bind the form first, then fold the result, passing a function to handle errors, and a function to handle succes.
    Application.queryForm.bindFromRequest.fold(
      // The error function. We return the index page with the error form, which will render the errors.
      // We also wrap the result in a successful future, since this action is synchronous, but we're required to return
      // a future because the query creation function returns a future.
      errorForm => {
        Ok(views.html.index("Error in Add Query Form."))
      },
      // There were no errors in the from, so create the query.
      query => {
        val (ldate, udate) = query.date match {
          case "now"  => (query.ld1, query.ud1)
          case "since"  => (query.ld2, query.ud3)
          case "period"  => (query.ld3, query.ud3)
        }
        val trend_value = query.trend match {
          case "topk"  => query.k
          case "threshold"  => query.t
        }
        val newQuery = Json.toJson(Map(
          "querytype" -> querytype,
          "lowerTime" -> ldate,
          "upperTime" -> udate,
          "trendType" -> query.trend,
          "value" -> trend_value
        ))       

        val responseFuture = wSClient.url(s"http://$queryHost:$queryPort/events/$event_name/$querytype/$lambdatype/query").post(newQuery)
        val response = Await.result(responseFuture, 100 seconds)
        val query_response = Json.parse(response.body).as[Query]
        queries.append(query_response) 
        Redirect(routes.Application.getEvent(event_name))
      }
    )
  }

def setQueryStatus(event_name: String, querytype: String, lambdatype: String, query_name: String) = Action { implicit request =>
   val formValidationResult = Application.setProcessForm.bindFromRequest
    formValidationResult.fold(
        // binding failure, you retrieve the form containing errors:
      errorForm => {
        Ok(views.html.index("Error in Set Query Status."))
      }, 
      process => {
        // binding success, you get the actual value. 
        val responseFuture = wSClient
          .url(s"http://$queryHost:$queryPort/events/${event_name}/$querytype/$lambdatype/query/${query_name}/${process.request}")
         .post("")
        val response = Await.result(responseFuture, 100 seconds)
        Redirect(routes.Application.getEvent(event_name))
      }
    )
  }

  def deleteQuery(event_name: String, querytype: String, lambdatype: String, query_name: String) = Action { implicit request =>
   val formValidationResult = Application.setProcessForm.bindFromRequest
    formValidationResult.fold(
        // binding failure, you retrieve the form containing errors:
      errorForm => {
        Ok(views.html.index("Error in Delete Query."))
      }, 
      request => {
        // binding success, you get the actual value. 
        //if (input.request == "delete"){
        val responseFuture = wSClient
          .url(s"http://$queryHost:$queryPort/events/${event_name}/$querytype/$lambdatype/query/${query_name}")
          .delete
        val response = Await.result(responseFuture, 100 seconds)
        Redirect(routes.Application.getEvent(event_name))
      }
    )
  }


}//end-contoller



object Application {

  /** The form definition for the "create a event" form.
   *  It specifies the form fields and their types,
   *  as well as how to convert from a Event to form data and vice versa.
   */
  val addEventForm = Form(
    mapping(
      "Name" -> nonEmptyText,
      "Description" -> text,
      "Start Date" -> text,
      "End Date" -> text,//ignored(23L),
      "Active" -> boolean
    )(Event.apply)(Event.unapply)
  )
  
  val addKeywordForm = Form(
    mapping(
      "Text" -> nonEmptyText//,
    )(Keyword.apply)(Keyword.unapply)
  )

  val addActiveForm = Form(
    mapping(
      "active" -> text//,
    )(Active.apply)(Active.unapply)
  )

  val setProcessForm = Form(
    mapping(
      "request" -> text//,
    )(ProcessRequest.apply)(ProcessRequest.unapply)
  )

  val queryForm: Form[CreateQueryForm] = Form {
    mapping(
      "querytype" -> text,
      "date" -> text,
      "ld1" -> text,
      "ud1" -> text,
      "ld2" -> text,
      "ud2" -> text,
      "ld3" -> text,
      "ud3" -> text,
      "trend" -> text,
      "k" -> text,
      "t" -> text
    )(CreateQueryForm.apply)(CreateQueryForm.unapply)
  }
}
