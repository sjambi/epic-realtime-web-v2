package models
import play.api.libs.json._
import play.api.libs.functional.syntax._
import play.api.data.validation.ValidationError

case class Process(
	event: String, 
	query_type: String,
	attribute: String,
	speed_job_running: Boolean,
	speed_job_id: String,
	batch_job_running: Boolean,
	batch_job_id: String
	)

object Process {
  implicit val ProcessFormat: Format[Process] = Json.format[Process]
  // implicit val ProcessDataReads = (
	 //  	(__ \ 'attribute).read[String] and
	 //  	(__ \ 'event_name).read[String] and
	 //  	(__ \ 'running).read[Boolean] 
  // 	)(Process.apply _)
}

case class ProcessRequest(
	request: String//,
	)

object ProcessRequest {
  implicit val ProcessRequestFormat: Format[ProcessRequest] = Json.format[ProcessRequest]
}
