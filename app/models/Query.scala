package models
import play.api.libs.json._
import play.api.libs.functional.syntax._
import play.api.data.validation.ValidationError

case class Query(
	name: String, 
	event: String, 
	querytype: String,
	lambdatype: String, 
	lowertime: String, 
	uppertime: String, 
	trendtype: String, 
	value: String, 
	running: Boolean,
	speed_job_id: String,
	batch_job_id: String,
	serving_job_id: String	
	)

object Query {
  implicit val QueryFormat: Format[Query] = Json.format[Query]
}

case class CreateQueryForm(
	querytype: String,
	date: String, 
	ld1: String, 
	ud1: String,
	ld2: String, 
	ud2: String,
	ld3: String, 
	ud3: String,
	trend: String,
	k: String, 
	t: String
)
