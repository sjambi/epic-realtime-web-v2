package models
import play.api.libs.json._
import play.api.libs.functional.syntax._
import play.api.data.validation.ValidationError

case class Active(
	active: String//,
	)

object Active {
  implicit val ActiveFormat: Format[Active] = Json.format[Active]
  // implicit val KeywordDataReads = (
	 //  	// (__ \ 'event).read[String] and
	 //  	(__ \ 'text).read[String] //and
	 //  	// (__ \ 'active).read[Boolean] 
  // 	)(Keyword.apply _)
}
