package models
import play.api.libs.json._
import play.api.libs.functional.syntax._
import play.api.data.validation.ValidationError


case class Event(
	name: String, 
	description: String, 
	startDate: String, //java.util.Date, 
	endDate: String, //java.util.Date, 
	isActive: Boolean)

object Event {
  implicit val EventFormat: Format[Event] = Json.format[Event]
  // implicit val EventDataReads = (
	 //  	(__ \ 'name).read[String] and
	 //  	(__ \ 'description).read[String] and
	 //  	(__ \ 'startDate).read[String] and
	 //  	(__ \ 'endDate).read[String] and
	 //  	(__ \ 'isActive).read[Boolean] 
  // 	)(Event.apply _)
}
