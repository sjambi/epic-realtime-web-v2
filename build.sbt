name := """play-forms-tutorial"""

version := "1.0-SNAPSHOT"

scalaVersion := "2.11.7"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

resolvers += "scalaz-bintray" at "http://dl.bintray.com/scalaz/releases"

//routesGenerator := InjectedRoutesGenerator

libraryDependencies ++= Seq(
  ws,
  cache,
  "com.typesafe.play" %% "play-ws" % "2.5",
  "com.datastax.cassandra" % "cassandra-driver-core" % "2.1.2"
  //"com.typesafe.play" %% "play-streams-experimental" % "2.5.x"
)

// Play provides two styles of routers, one expects its actions to be injected, the
// other, legacy style, accesses its actions statically.
routesGenerator := InjectedRoutesGenerator

PlayKeys.playOmnidoc := false

//libraryDependencies += filters

